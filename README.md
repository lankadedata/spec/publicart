# Applikationsprofil för offentlig konst

Denna specifikation genereras via RDForms utifrån metadataprofiler som används för redigering i EntryScape och presentation på [statenskonstrad.se/arkiv-over-offentlig-konst](http://statenskonstrad.se/arkiv-over-offentlig-konst/).

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.0-4baaaa.svg)](CODE_OF_CONDUCT.md) 
